package com.pillar.kata;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class RomanNumeralTest {
	
	RomanNumeral romanNumeral;
	String expectedValue;
	String actualValue;
	
	@Before
	public void setUp() {
		romanNumeral = new RomanNumeral();
		expectedValue = "";
		actualValue = "";
	}
	
	@Test
	public void test_convert_arabic_1_to_roman_returns_i() {
		expectedValue = "I";
		actualValue = romanNumeral.convert(1);
		assertTrue(expectedValue.equals(actualValue));
	}

	@Test
	public void test_convert_arabic_2_to_roman_returns_ii() {
		expectedValue = "II";
		actualValue = romanNumeral.convert(2);
		assertTrue(expectedValue.equals(actualValue));
	}
	
	@Test
	public void test_convert_arabic_3_to_roman_returns_iii() {
		expectedValue = "III";
		actualValue = romanNumeral.convert(3);
		assertTrue(expectedValue.equals(actualValue));
	}
	
	@Test
	public void test_convert_arabic_4_to_roman_returns_iv() {
		expectedValue = "IV";
		actualValue = romanNumeral.convert(4);
		assertTrue(expectedValue.equals(actualValue));
	}
	
	@Test
	public void test_convert_arabic_5_to_roman_returns_v() {
		expectedValue = "V";
		actualValue = romanNumeral.convert(5);
		assertTrue(expectedValue.equals(actualValue));
	}
	
	@Test
	public void test_convert_arabic_6_to_roman_returns_vi() {
		expectedValue = "VI";
		actualValue = romanNumeral.convert(6);
		assertTrue(expectedValue.equals(actualValue));
	}
	
	@Test
	public void test_convert_arabic_7_to_roman_returns_vii() {
		expectedValue = "VII";
		actualValue = romanNumeral.convert(7);
		assertTrue(expectedValue.equals(actualValue));
	}
	
	@Test
	public void test_convert_arabic_8_to_roman_returns_viii() {
		expectedValue = "VIII";
		actualValue = romanNumeral.convert(8);
		assertTrue(expectedValue.equals(actualValue));
	}

	@Test
	public void test_convert_arabic_9_to_roman_returns_ix() {
		expectedValue = "IX";
		actualValue = romanNumeral.convert(9);
		assertTrue(expectedValue.equals(actualValue));
	}
	
	@Test
	public void test_convert_arabic_10_to_roman_returns_x() {
		expectedValue = "X";
		actualValue = romanNumeral.convert(10);
		assertTrue(expectedValue.equals(actualValue));
	}

	@Test
	public void test_convert_arabic_11_to_roman_returns_xi() {
		expectedValue = "XI";
		actualValue = romanNumeral.convert(11);
		assertTrue(expectedValue.equals(actualValue));
	}

	@Test
	public void test_convert_arabic_12_to_roman_returns_xii() {
		expectedValue = "XII";
		actualValue = romanNumeral.convert(12);
		assertTrue(expectedValue.equals(actualValue));
	}

	@Test
	public void test_convert_arabic_13_to_roman_returns_xiii() {
		expectedValue = "XIII";
		actualValue = romanNumeral.convert(13);
		assertTrue(expectedValue.equals(actualValue));
	}

	@Test
	public void test_convert_arabic_14_to_roman_returns_xiv() {
		expectedValue = "XIV";
		actualValue = romanNumeral.convert(14);
		assertTrue(expectedValue.equals(actualValue));
	}
	
	@Test
	public void test_convert_arabic_15_to_roman_returns_xiv() {
		expectedValue = "XV";
		actualValue = romanNumeral.convert(15);
		assertTrue(expectedValue.equals(actualValue));
	}

}
