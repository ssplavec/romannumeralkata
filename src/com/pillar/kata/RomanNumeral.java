package com.pillar.kata;

public class RomanNumeral {

	public String convert(int arabic) {
		String roman = "";
		
		if (arabic > 10) {
			roman = "X";
			arabic = arabic - 10;
		}
		
		if (arabic == 10) {
			return "X";
		} else if (arabic == 9) {
			return "IX";
		}
		
		if (arabic > 5) {
			roman += "V";
			arabic = arabic - 5;
		}
		
		if (arabic == 5) {
			roman += "V";
		} else if (arabic == 4) {
			roman += "IV";
		} else {
			StringBuilder romanSB = new StringBuilder();
			for (int i = 0; i < arabic; i++) {
				romanSB.append("I");
			}
			roman += romanSB.toString();
		}
		
		return roman;
	}

}
